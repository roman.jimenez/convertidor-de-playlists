dale permiso a el documento para ejecutarse
``` sh
chmod +x playlist.rb
```

se ejecuta de esta manera:

(necesitas dar el url del playlist con el proveedor de la playlist en el URL)

(playlist de ejemplo https://deezer.com/playlist/908622995)
``` sh
./playlist.rb 'https://deezer.com/playlist/908622995'
```


yo uso un archivo llamado `tokens.rb`  para guardar mis ~secretos~ en vez de una DB porque xd (esta en TODO si alguien lee esto y lo pide lo hago sin p2)
``` ruby
def _spotify_client_id
  return ''
end
def _spotify_client_secret
  return ''
end
def _spotify_user_id
  return ''
end
def _spotify_refresh_token
  return ''
end
def _deezer_app_id
  return ''
end
def _deezer_secret_key
  return ''
end
def _deezer_access_token
  return ''
end
def _deezer_user_id
  return ''
end
```
