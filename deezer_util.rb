require_relative 'tokens'
require 'json'
require 'net/http'

@deezer_uri_base = 'https://api.deezer.com/'

def deezer_buscar_playlist id_de_playlist
  d_uri = URI("#{@deezer_uri_base}/playlist/#{id_de_playlist}")
  respuesta = Net::HTTP.get(d_uri)
  playlist = JSON.parse(respuesta)
  playlist
end

def deezer_crear_playlist nombre
  d_uri = URI("#{@deezer_uri_base}/user/#{_deezer_user_id}/playlists?title=#{nombre}&access_token=#{_deezer_access_token}")
  http = Net::HTTP.new(d_uri.hostname, d_uri.port)
  req = Net::HTTP::Post.new(d_uri)
  http.use_ssl = true

  d_respuesta = http.request(req)
  deezer_playlist_id = JSON.parse(d_respuesta.body)
  deezer_playlist_id['id']
end

def deezer_llenar_playlist (id_de_playlist, playlist_original)
  canciones = []

  playlist_original.each do |data|
    busqueda = URI.encode("artist:\"#{data['track']['artists'][0]['name']}\" track:\"#{data['track']['name']}\"")
    sp_busqueda_uri = URI("#{@deezer_uri_base}/search?q=#{busqueda}&access_token=#{_deezer_access_token}")
    req = Net::HTTP::Get.new(sp_busqueda_uri)
    http = Net::HTTP.new(sp_busqueda_uri.hostname, sp_busqueda_uri.port)
    http.use_ssl = true
    sp_respuesta = http.request(req)
    resultado = JSON.parse( sp_respuesta.body )
    if resultado['data'][0].nil?
      puts "No se encontro una cancion similar a #{data['track']['name']}"
      puts sp_busqueda_uri
    else
      canciones.push(resultado['data'][0]['id'])
      puts "#{ resultado['data'][0]['title'] } añadida a lista"
    end
  end

  d_uri = URI("#{@deezer_uri_base}/playlist/#{id_de_playlist}/tracks?access_token=#{_deezer_access_token}&songs=#{canciones.join(',')}")
  http = Net::HTTP.new(d_uri.hostname, d_uri.port)
  req = Net::HTTP::Post.new(d_uri)
  http.use_ssl = true

  http.request(req)
end
