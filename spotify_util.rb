require_relative 'tokens'
require 'json'
require 'net/http'

@spotify_uri_base = 'https://api.spotify.com/v1'

def generar_token_spotify
  # siempre hay uno que TIENE que ser especial ¬¬
  sp_uri = URI("https://accounts.spotify.com/api/token")
  http = Net::HTTP.new(sp_uri.hostname, sp_uri.port)
  req = Net::HTTP::Post.new(sp_uri)

  http.use_ssl = true
  req.basic_auth(_spotify_client_id, _spotify_client_secret)
  req.body = URI.encode_www_form( {'grant_type':'refresh_token', 'refresh_token': _spotify_refresh_token} )

  sp_respuesta = http.request(req)

  json = JSON.parse( sp_respuesta.body )
  json['access_token']
end

def spotify_crear_playlist(token_spotify, nombre)
  sp_uri = URI("#{@spotify_uri_base}/users/#{_spotify_user_id}/playlists")
  req = Net::HTTP::Post.new(sp_uri, 'Content-Type' => 'application/json')
  req['Authorization'] = "Bearer #{ token_spotify }"
  req.body = {
    "name": nombre,
    "description": "Copia de una playlist de Deezer",
    "public": "false"
  }.to_json

  http = Net::HTTP.new(sp_uri.hostname, sp_uri.port)
  http.use_ssl = true
  sp_respuesta = http.request(req)
  resultado = JSON.parse( sp_respuesta.body )
  resultado['id']
end

def spotify_encontrar_playlist (token_spotify, id_de_playlist )
  sp_busqueda_uri = URI("#{@spotify_uri_base}/playlists/#{id_de_playlist}")
  req = Net::HTTP::Get.new(sp_busqueda_uri)
  req['Authorization'] = "Bearer #{token_spotify}"
  http = Net::HTTP.new(sp_busqueda_uri.hostname, sp_busqueda_uri.port)
  http.use_ssl = true
  sp_respuesta = http.request(req)
  resultado = JSON.parse( sp_respuesta.body )
  { name: resultado['name'] , tracks: resultado['tracks']['items'] }
end

def spotify_llenar_playlist(token_spotify, id_de_playlist, playlist_original)
  canciones = []

  playlist_original['tracks']['data'].each do |track|
    busqueda = URI.encode("#{ track['title'] } #{track['artist']['name']}")
    sp_busqueda_uri = URI("#{@spotify_uri_base}/search?q=#{busqueda}&type=track&limit=1")
    req = Net::HTTP::Get.new(sp_busqueda_uri)
    req['Authorization'] = "Bearer #{token_spotify}"
    http = Net::HTTP.new(sp_busqueda_uri.hostname, sp_busqueda_uri.port)
    http.use_ssl = true
    sp_respuesta = http.request(req)
    resultado = JSON.parse( sp_respuesta.body )
    if resultado.nil?
      puts "No se encontro una cancion similar a #{track['title']}"
    else
      resultado['tracks']['items'].each do |item|
        canciones.push("spotify:track:#{item['id']}")
        puts "#{ track['title'] } #{ item['id'] } añadida a la lista!"
      end
    end
  end

  sp_playlist_uri = URI("#{@spotify_uri_base}/playlists/#{id_de_playlist}/tracks?uris=#{canciones.join(',')}")
  req = Net::HTTP::Post.new(sp_playlist_uri)
  req['Authorization'] = 'Bearer ' + token_spotify
  http = Net::HTTP.new(sp_playlist_uri.hostname, sp_playlist_uri.port)
  http.use_ssl = true
  sp_añadida_respuesta = http.request(req)
  resultado = JSON.parse( sp_añadida_respuesta.body )
  puts resultado
end
