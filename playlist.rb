#!/usr/bin/ruby

require_relative 'deezer_a_spotify'
require_relative 'spotify_a_deezer'

id_de_playlist = ARGV[0]

origen = 'spotify' if id_de_playlist.include? 'spotify'
origen = 'deezer' if id_de_playlist.include? 'deezer'

# limpia el input
id_de_playlist = id_de_playlist.slice(0..(id_de_playlist.index('?') - 1)) if id_de_playlist.include? '?'
id_de_playlist = id_de_playlist.split('/').last if id_de_playlist.include? '/'
id_de_playlist = id_de_playlist.strip

case origen
when 'deezer'
  deezer_a_spotify(id_de_playlist)
when 'spotify'
  spotify_a_deezer(id_de_playlist)
else
  puts 'Opcion invalida'
  return 0
end

##http://patorjk.com/software/taag
puts <<-'EOF'
U|  _"\ u  |"|    U  /"\  u  \ \ / / |"|        ___    / __"| u |_ " _|
\| |_) |/U | | u   \/ _ \/    \ V /U | | u     |_"_|  <\___ \/    | |
 |  __/   \| |/__  / ___ \   U_|"|_u\| |/__     | |    u___) |   /| |\
 |_|       |_____|/_/   \_\    |_|   |_____|  U/| |\u  |____/>> u |_|U
 ||>>_     //  \\  \\    >>.-,//|(_  //  \\.-,_|___|_,-.)(  (__)_// \\_
 (__)__)   (_")("_)(__)  (__)\_) (__)(_")("_)\_)-' '-(_/(__)    (__) (__)
    ____    ____    U _____ u    _      ____       _
 U /"___|U |  _"\ u \| ___"|/U  /"\  u |  _"\  U  /"\  u
 \| | u   \| |_) |/  |  _|"   \/ _ \/ /| | | |  \/ _ \/
  | |/__   |  _ <    | |___   / ___ \ U| |_| |\ / ___ \
   \____|  |_| \_\   |_____| /_/   \_\ |____/ u/_/   \_\
 _// \\   //   \\_  <<   >>  \\    >>  |||_    \\    >>
(__)(__) (__)  (__)(__) (__)(__)  (__)(__)_)  (__)  (__)   "
EOF
