require_relative 'deezer_util'
require_relative 'spotify_util'

require 'net/http'
require 'json'

def spotify_a_deezer id_de_playlist

  token_spotify = generar_token_spotify
  canciones_de_playlist = spotify_encontrar_playlist(token_spotify, id_de_playlist)
  puts "#{ canciones_de_playlist[:name] } sera copiada a Deezer"

  #####
  # Crea un playlist en Deezer con el mismo nombre y las canciones que coincidan
  #####
  deezer_playlist_id = deezer_crear_playlist(canciones_de_playlist[:name])
  # deezer_playlist_id = 1
  deezer_llenar_playlist(deezer_playlist_id, canciones_de_playlist[:tracks])
end
