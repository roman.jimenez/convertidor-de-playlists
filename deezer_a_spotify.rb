#!/usr/bin/ruby

require_relative 'deezer_util'
require_relative 'spotify_util'

require 'net/http'
require 'json'

def deezer_a_spotify id_de_playlist
  playlist = deezer_buscar_playlist(id_de_playlist)
  puts "#{ playlist['title'] } sera copiada a Spotify"
  #####
  # Crea un playlist en Spotify con el mismo nombre y las canciones que coincidan
  #####
  token_spotify = generar_token_spotify
  spotify_playlist_id = spotify_crear_playlist(token_spotify, playlist['title'])
  spotify_llenar_playlist(token_spotify, spotify_playlist_id, playlist)
end
